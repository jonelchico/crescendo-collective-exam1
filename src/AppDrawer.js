import React, { useState, useEffect } from 'react';
import { Drawer, Hidden, Grid, Typography, List, ListItem, ListItemIcon, ListItemText } from '@material-ui/core';
import { makeStyles } from '@material-ui/styles';
import { withRouter } from 'react-router-dom';
import logo from './assets/cc_logo.png';
import { Filter1, Filter2 } from '@material-ui/icons';

const useStyles = makeStyles(theme => ({
    drawer: {
        [theme.breakpoints.up('sm')]: {
            width: 255,
            flexShrink: 0,
        },
    },
    drawerPaper: {
        width: 255,
        zIndex: 1,
        backgroundColor: theme.palette.primary.main
    },
    logo: {
        margin: 0,
        width: theme.spacing(4),
        height: theme.spacing(4),
    },
    logoName: {
        width: theme.spacing(12),
        fill: theme.palette.white.main
    },
    logoContainer: {
        padding: theme.spacing(1)
    },
    logoContainerHover: {
        padding: theme.spacing(1),
        cursor: 'pointer'
    },
    listContainer: {
        width: '100%',
    },
    listItemContainer: {
        marginTop: theme.spacing(1),
        marginBottom: theme.spacing(1),
    },
    listItemIcon: {
        maxWidth: 20,
        paddingLeft: theme.spacing(2)
    },
    listItemText: {
        fontSize: '16px',
    },
    icon: {
        fill: theme.palette.orange.main,
        width: 18,
        height: 18
    },
    iconSelected: {
        fill: theme.palette.white.main,
        width: 18,
        height: 18
    },
    menuItemSelected: {
        backgroundColor: 'rgba(0, 0, 0, 0.1)',
        borderLeft: '5px solid #EC5E2C',
    },
    iconContainer: {
        marginLeft: '-5px'
    }


}));

export default withRouter(function ResponsiveDrawer(props) {
    const {  history, location } = props;
    const classes = useStyles();

    const mainMenuPath = `/${location.pathname.split('/')[1]}`;
    const [selectedMainMenuItem, setSelectedMainMenuItem] = useState(`${mainMenuPath}`);

    function pushHistory(url) {
        history.push(url)
    }

    const MyListItem = ({ text, action, listItemIcon, selected, classes }) => {
        let className = selected ? classes.menuItemSelected : '';
        let iconContainerCondition = selected ? classes.iconContainer : null
        return <Grid container className={classes.listItemContainer} >
            <ListItem
                className={className}
                button
                divider={false}
                onClick={action}
            >
                <Grid container direction="row" alignItems="center" justify="space-between" >
                    <Grid item xs={9} >
                        <Grid className={iconContainerCondition} container direction="row" alignItems="center" justify="space-evenly" >
                            <Grid item xs={2} container justify="center">
                                {listItemIcon}
                            </Grid>
                            <Grid item xs={3}>
                                <ListItemText  >
                                    <Typography className={classes.listItemText} variant="body2">{text}</Typography>
                                </ListItemText>
                            </Grid>
                        </Grid>
                    </Grid>
                    <Grid item xs={2}>
                    </Grid>
                </Grid>
            </ListItem>
        </Grid>
    }

    const drawer = (
        <>
            <Grid container direction="column" alignItems="center" style={{ minHeight: '100vh', }}>
                <Grid item container direction="column" alignItems="center" className={classes.logoContainer}>
                    <Grid item className={classes.logoContainerHover} onClick={() => pushHistory("/")} >
                        <img alt="Crescendo Collective" src={logo} className={classes.logoName} />
                    </Grid>
                </Grid>
                <Grid item className={classes.listContainer} >
                    <List disablePadding={true} >
                        <MyListItem
                            selected={selectedMainMenuItem === '/recipes'}
                            classes={classes}
                            text="Recipes"
                            listItemIcon={
                                <ListItemIcon className={classes.listItemIcon}>
                                    {
                                        selectedMainMenuItem === './recipes' ?
                                            <Filter1 className={classes.iconSelected} /> :
                                            <Filter1 className={classes.icon} />
                                    }
                                </ListItemIcon>
                            }
                            action={() => pushHistory('/recipes')} />
                        <MyListItem
                            selected={selectedMainMenuItem === '/specials'}
                            classes={classes}
                            text="Specials"
                            listItemIcon={
                                <ListItemIcon className={classes.listItemIcon}>
                                    {
                                        selectedMainMenuItem === './specials' ?
                                            <Filter2 className={classes.iconSelected} /> :
                                            <Filter2 className={classes.icon} />
                                    }
                                </ListItemIcon>
                            }
                            action={() => pushHistory('/specials')} />
                    </List>
                </Grid>
            </Grid>
        </>
    );

    useEffect(() => {
        if (selectedMainMenuItem !== mainMenuPath) {
            setSelectedMainMenuItem(mainMenuPath)
        }
    }, [mainMenuPath])

    return (
        <nav className={classes.drawer} aria-label="App navigations" id="noScrollBar">           
            <Hidden xsDown>
                <Drawer
                    classes={{
                        paper: classes.drawerPaper
                    }}
                    variant="persistent"
                    open={true}
                >
                    {drawer}
                </Drawer>
            </Hidden>
        </nav>
    )
});