import React, { useState, useEffect } from 'react';
import { Grid } from '@material-ui/core';
import { makeStyles, withStyles } from '@material-ui/styles';
import CircularProgress from '@material-ui/core/CircularProgress';
import Card from './common/CardSpecial'

const useStyles = makeStyles(theme => ({
    loadingContainer: {
        height: theme.spacing(50)
    }
}))

const ColorCircularProgress = withStyles({
    root: {
        color: '#EC5E2C',
    },
})(CircularProgress);


export const Specials = () => {
    const classes = useStyles();
    const [specials, setSpecials] = useState(null);
    const apiUrl = process.env.REACT_APP_API_HOST;


    const fetchData = async () => {
        const response = await fetch(apiUrl + '/specials')

        const data = await response.json()
        if (data) {
            return setSpecials(data)
        }

    }

    const FetchingData = () => <Grid item container className={classes.loadingContainer} alignItems="center" justify="center">
        <ColorCircularProgress />
    </Grid>

    useEffect(() => {
        fetchData()
    }, [])

    return (
        <Grid container spacing={2} >
            {
                specials ? specials.map((special) => {
                    return (
                            <Card key={special.uuid} data={special} />
                    )
                }) : <FetchingData />
            }
        </Grid>
    )
};
