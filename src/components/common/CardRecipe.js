import React from 'react';
import { makeStyles, withStyles } from '@material-ui/core/styles';
import { Grid, Typography, } from '@material-ui/core';
import { ExpandMore } from '@material-ui/icons'
import MuiExpansionPanel from '@material-ui/core/ExpansionPanel';
import MuiExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
import MuiExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails';

const ExpansionPanel = withStyles({
    root: {
        border: '1px solid rgba(0, 0, 0, .125)',
        boxShadow: 'none',
        '&:not(:last-child)': {
            borderBottom: 0,
        },
        '&:before': {
            display: 'none',
        },
        '&$expanded': {
            margin: 'auto',
        },
    },
    expanded: {},
})(MuiExpansionPanel);

const ExpansionPanelSummary = withStyles({
    root: {
        backgroundColor: 'rgba(0, 0, 0, .03)',
        borderBottom: '1px solid rgba(0, 0, 0, .125)',
        marginBottom: -1,
        minHeight: 56,
        '&$expanded': {
            minHeight: 56,
        },
    },
    content: {
        '&$expanded': {
            margin: '12px 0',
        },
    },
    expanded: {},
})(MuiExpansionPanelSummary);

const ExpansionPanelDetails = withStyles((theme) => ({
    root: {
        padding: theme.spacing(2),
    },
}))(MuiExpansionPanelDetails);

const useStyles = makeStyles((theme) => ({
    root: {
        width: '100%',
        marginBottom: theme.spacing(2)
    },
    expand: {
        transform: 'rotate(0deg)',
        marginLeft: 'auto',
        transition: theme.transitions.create('transform', {
            duration: theme.transitions.duration.shortest,
        }),
    },
    expandOpen: {
        transform: 'rotate(180deg)',
    },
    heading: {
        fontSize: theme.typography.pxToRem(18),
        flexBasis: '33.33%',
        fontWeight: [600],
        flexShrink: 0,
    },
    secondaryHeading: {
        fontSize: theme.typography.pxToRem(18),
        color: theme.palette.text.secondary,
    },
    subContent: {
        marginTop: theme.spacing(1),
        marginBottom: theme.spacing(1)
    },
    ingredient: {
        fontWeight: [600]
    },
}));

export default function CardRecipe({ data, url }) {
    const classes = useStyles();
    const [expanded, setExpanded] = React.useState(false);

    const { uuid, title, description, images, servings, prepTime, cookTime, ingredients, directions } = data


    const handleChange = (panel) => (event, isExpanded) => {
        setExpanded(isExpanded ? panel : false);
    };

    return (
        <div className={classes.root}>
            <ExpansionPanel expanded={expanded === uuid} onChange={handleChange(uuid)}>
                <ExpansionPanelSummary
                    expandIcon={<ExpandMore />}
                    aria-controls="panel1bh-content"
                    id="panel1bh-header"
                >
                    <Typography className={classes.heading}>{title}</Typography>
                </ExpansionPanelSummary>
                <ExpansionPanelDetails>
                    <Grid container direction="column" spacing={1}>
                        <Grid item container spacing={2}>
                            <Grid item xs={12} md={3}>
                                <img src={url + images.medium} alt={title} width="100%" />
                            </Grid>
                            <Grid itemxs={12} md={9}>
                                <Typography className={classes.secondaryHeading}>{description}</Typography>
                                <Grid container className={classes.subContent}>
                                    <Grid item xs={12}><Typography>Servings: {servings}</Typography></Grid>
                                    <Grid item xs={12}><Typography>Perparation Time: {prepTime}</Typography></Grid>
                                    <Grid item xs={12}><Typography>Cooking Time: {cookTime}</Typography></Grid>
                                </Grid>
                            </Grid>
                        </Grid>

                        <Grid item container direction="column">
                            <Grid item><Typography className={classes.ingredient}>Ingredients:</Typography></Grid>
                            <Grid item>
                                <Grid container>
                                    {
                                        ingredients.map((ingredient) => {
                                            const { name, amount, uuid, measurement } = ingredient
                                            return (
                                                <Grid key={uuid} item xs={12}  md={4} >
                                                    <Grid container >
                                                        <Grid item container alignItems="center">
                                                            <Typography variant="caption" style={{ fontWeight: 'bold' }}>{name}</Typography>
                                                        </Grid>
                                                        <Grid item container direction="column" style={{ paddingLeft: 15 }}>
                                                            <Typography variant="caption">Measurements: {measurement}</Typography>
                                                            <Typography gutterBottom variant="caption">Amount: {amount}</Typography>
                                                        </Grid>
                                                    </Grid>
                                                </Grid>
                                            )
                                        })
                                    }
                                </Grid>
                            </Grid>
                        </Grid>

                        <Grid item container direction="column">
                            <Grid item><Typography className={classes.ingredient}>Directions:</Typography></Grid>
                            <Grid item>
                                <Grid container>
                                    {
                                        directions.map((direction, index) => {
                                            const { instructions } = direction
                                            return (
                                                <Grid key={index} itemxs={12} md={12} >
                                                    <Grid container >
                                                        <Grid item alignItems="center">
                                                            <Typography variant="caption" style={{ fontWeight: 'bold', minWidth: 10, marginRight: 15 }}>{index+1}.</Typography>
                                                            <Typography variant="caption" style={{ fontWeight: 'bold' }}>{instructions}</Typography>
                                                        </Grid>
                                                    </Grid>
                                                </Grid>
                                            )
                                        })
                                    }
                                </Grid>
                            </Grid>
                        </Grid>
                    </Grid>
                </ExpansionPanelDetails>
            </ExpansionPanel>


        </div >
    );
}