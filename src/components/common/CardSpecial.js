import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { Grid, Card, CardActions, CardContent, Button, Typography } from '@material-ui/core'

const useStyles = makeStyles(theme => ({
    root: {
        minWidth: '100%',
    },
    title: {
        fontSize: 14,
    },
    contentContainer: {
        marginBottom: -theme.spacing(2)
    },
    pos: {
        marginBottom: 12,
    },
}));

export default function OutlinedCard({ data }) {
    const classes = useStyles();
    const { text, type, title } = data

    return (
        <Grid item xs={12} lg={5}>

            <Card className={classes.root} variant="outlined">
                <CardContent className={classes.contentContainer}>
                    <Typography variant="h5" component="h2">
                        {title}
                    </Typography>
                    <Typography className={classes.pos} color="textSecondary">
                        {type[0].toUpperCase()+type.slice(1)} Event    
                    </Typography>
                    <Typography variant="body2" component="p">
                        <div dangerouslySetInnerHTML={{ __html: text }} />
                    </Typography>
                </CardContent>
                <CardActions>
                    <Button size="small">Learn More</Button>
                </CardActions>
            </Card>
        </Grid>
    );
}