import React, { useState, useEffect } from 'react';
import { Grid } from '@material-ui/core';
import { makeStyles, withStyles } from '@material-ui/styles';
import CircularProgress from '@material-ui/core/CircularProgress';
import Card from './common/CardRecipe'

const useStyles = makeStyles(theme => ({
    loadingContainer: {
        height: theme.spacing(50)
    }
}))

const ColorCircularProgress = withStyles({
    root: {
        color: '#EC5E2C',
    },
})(CircularProgress);


export const Recipes = () => {
    const classes = useStyles();
    const [recipes, setRecipes] = useState(null);
    const apiUrl = process.env.REACT_APP_API_HOST;


    const fetchData = async () => {
        const response = await fetch(apiUrl + '/recipes')

        const data = await response.json()
        if (data) {
            return setRecipes(data)
        }

    }

    const FetchingData = () => <Grid item container className={classes.loadingContainer} alignItems="center" justify="center">
        <ColorCircularProgress />
    </Grid>

    useEffect(() => {
        fetchData()
    }, [])

    return (
        <Grid container spacing={2} >
            {
                recipes ? recipes.map((recipe) => {
                    return (
                            <Card data={recipe} url={apiUrl} key={recipe.uuid} />
                    )
                }) : <FetchingData />
            }
        </Grid>
    )
};
