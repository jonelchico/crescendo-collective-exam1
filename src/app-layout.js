import React from 'react';
import { Route, Switch, withRouter } from 'react-router-dom';
import { Recipes, Specials } from './components';
import { makeStyles } from '@material-ui/styles';
import AppDrawer from './AppDrawer';

const useStyles = makeStyles(theme => ({
    root: {
        display: 'flex',
    },
    content: {
        display: 'flex',
        flexDirection: 'column',
        minHeight: '100vh',
        flexGrow: 1,
        padding: theme.spacing(2),
    },
}))


export default withRouter(function AppRoutes(props) {
    
    const classes = useStyles();

    return (
        <div className={classes.root}>
            <AppDrawer />
            <main className={classes.content}>
                <Switch>
                    <Route exact path={"/"} component={Recipes} />
                    <Route path={"/recipes"} component={Recipes} />
                    <Route path={"/specials"} component={Specials} />
                </Switch>
            </main>
        </div>
    );
})